import React from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router';

export default function ViewCategory() {
    const navigate = useNavigate();
    return (
        <div>
            <Container className='mt-2'>
                  <Button variant="warning" className='mt-1' onClick={() => { navigate('/') }}>Back</Button>
                <Row className='mt-2'>
                  
                    <Col>
                        <h6 >Title :</h6>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
