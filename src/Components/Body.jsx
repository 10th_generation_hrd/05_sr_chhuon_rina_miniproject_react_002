import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { Carousel, Col, Container, Row } from 'react-bootstrap'
import AllArticles from './AllArticles'
import CardComponents from './CardComponents'
import {myApi} from"../api/myApi"

export default function Body(item) {
    const [data, setData] = useState([]);

    useEffect(() => {
        myApi.get("/articles").then((res) => {
          setData(res.data.payload);
        });
      },);

      const handleDelete = (id) => {
        const newData = data.filter(data => data._id !== id)
        setData(newData)
        myApi.delete(`/articles/${id}`).then((r) => {console.log(r.data.message);})
      }
      const handleView = (id) =>{
          myApi.get(`/articles/${id}`).then((r) => {console.log(r.data.message);})
      }

  return (
    <Container style={{ height: '2000px' }}>
    <Carousel fade>
        <Carousel.Item>
            <img
                className="d-block w-100 "
                src="https://th.bing.com/th/id/R.55ebf88fcbe28fe52059b7945542d4ac?rik=4DiNoL9vby3Upg&pid=ImgRaw&r=0" height={"600px"}
                alt="First slide"
            />
        </Carousel.Item>
        <Carousel.Item>
            <img
                className="d-block w-100 "
                src="https://th.bing.com/th/id/R.bc71c1c1c50551a1d65e7b529ea29d08?rik=EU42gCFH4J%2bBZA&riu=http%3a%2f%2fwww.goodworklabs.com%2fwp-content%2fuploads%2f2016%2f10%2freactjs.png&ehk=qvQ5EVoUnJZ7k5L347zsU3f87YTckr1iQBzKdwXJd0w%3d&risl=&pid=ImgRaw&r=0" height={"600px"}
                alt="Second slide"
            />
        </Carousel.Item>
        <Carousel.Item>
            <img
                className="d-block w-100 "
                src="https://th.bing.com/th/id/R.b0ab36b7d09638b582320cf238594229?rik=fh3VdZAmdRMG1A&riu=http%3a%2f%2fvpsland.com%2fblog%2fwp-content%2fuploads%2f2016%2f04%2flinux-vps-benefits-g.jpg&ehk=GLvk0u9YdoOFDcVILhEqfSi0%2fY0rq12k9vJIWHLL9E0%3d&risl=&pid=ImgRaw&r=0" height={"600px"}
                alt="Third slide"
            />
        </Carousel.Item>
    </Carousel>

    <Row>
        <AllArticles/>
        <Row>{data.map((item, index)=>(
            <Col key={index} className='col-2'>
                <CardComponents  item={item} handleDelete={handleDelete}/>          
            </Col>))}
        </Row>

    </Row>
</Container>
  )
}
