import React from 'react'
import { useState } from 'react'
import { Button, Card, Container, FloatingLabel, Form } from 'react-bootstrap'
import { myApi } from '../api/myApi';

export default function CreatePost() {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState("");
    const [imageUrl,setImageUrl] = useState('');
    

    const handleSubmit = () => {
        myApi.post("/articles",{title,description,image})
            .then((response) => console.log(response.data.message))
    }
    
    const handleTitle = (a) => {
        setTitle(a.target.value)
    }

    const handleDescription = (e) => {
        setDescription(e.target.value)
    }

    const handleImage = (e) => {
        console.log(e.target.files[0])
        const formData = new FormData();
        console.log("Form Data : ", typeof(formData));
        formData.append("image",e.target.files[0]);
        myApi.post("/images",formData).then( (f)=>{
            console.log(f.data.payload.url)
            setImage(f.data.payload.url)
        })
        console.log(URL.createObjectURL(e.target.files[0]));
        setImageUrl(URL.createObjectURL(e.target.files[0]))
    }

  return (
    
        <Container style={{ height: '1000px' }}>
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" onChange={handleTitle} placeholder="Enter Title" />
                </Form.Group>
                <FloatingLabel controlId="floatingTextarea2" label="Description">
                    <Form.Control
                        onChange={handleDescription}
                        as="textarea"
                        placeholder="Leave a comment here"
                        style={{ height: '100px' }}
                    />
                </FloatingLabel>
                <Form.Group controlId="formFileMultiple" className="mt-3">
                    <Form.Control onChange={handleImage} type="file" multiple />
                </Form.Group>
                <img src={imageUrl ?? "https://raraavis-group.com/api/thumbor/unsafe/640x768/raraavis-group.com/uploads/product_image/image/491/qJLLZY4TNJEF4Y7h8zBqUQ.jpg"} alt="preview" style={{height:"150px"}}/>
                
                <Button className='mt-3'  onClick={handleSubmit} variant="primary">
                    Submit
                </Button>
            </Form>

        </Container>
   
  )
}
