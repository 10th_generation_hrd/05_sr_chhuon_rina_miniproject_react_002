import React from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import { Navigate, useLocation, useNavigate } from 'react-router';


export default function View() {
    const location = useLocation();
    const viewDate = location.state;
    const navigate = useNavigate();


    return (
        <Container className='mt-2'>
            <Button variant="warning" className='mt-1' onClick={() => {navigate('/')}}>Back</Button>
            <Row className='mt-2'>
            
                <Col>
                    <img  src={"https://raraavis-group.com/api/thumbor/unsafe/640x768/raraavis-group.com/uploads/product_image/image/491/qJLLZY4TNJEF4Y7h8zBqUQ.jpg"} style={{height:"450px", width:"100%"}}/>
                </Col>    
                <Col>
                    <h6 >Title :</h6>
                    <h6>Description :</h6>
                    <h6>Published :</h6>
                </Col>    
            </Row>         
        </Container>

    )
}


