import React from 'react'
import { Button, Card, Container } from 'react-bootstrap'
import { Navigate, useLocation, useNavigate } from 'react-router';

export default function CardComponents({item, handleDelete}) {
    const navigate = useNavigate();

    return (     
       <Container className='mt-2 '>
            <Card style={{width:"12rem "}}>
                <Card.Img variant="top"  src={item.image ?? "https://raraavis-group.com/api/thumbor/unsafe/640x768/raraavis-group.com/uploads/product_image/image/491/qJLLZY4TNJEF4Y7h8zBqUQ.jpg"} style={{height:"150px", width:"100%"}}/>
                <Card.Body>
                    <Card.Title style={{ textAlign: 'center' }}>{item.title}</Card.Title>
                    <Card.Text style={{ textAlign: 'center' }}>
                        {item.description}
                    </Card.Text>
                <Button variant="primary" className='mt-1'>Edit</Button>
                <Button variant="warning" className='mt-1'onClick={() => {navigate('/articles/view')}}>View</Button>
                <Button variant="danger" className='mt-1' onClick={() => handleDelete(item._id)} >Delete</Button>
                </Card.Body>
            </Card>
       </Container>
    )
}
