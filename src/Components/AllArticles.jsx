import React from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import { useNavigate } from 'react-router';

export default function AllArticles() {
    const navigate = useNavigate();
    return (
        <div>
            <Container className='mt-2'>
                <Row> 
                    <Col><h2>All Article</h2></Col>
                    <Col style={{textAlign:"end"}}>
                         <Button variant="outline-dark" onClick={() => {navigate('/articles/createpost')}}><h5>New Article</h5></Button>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
