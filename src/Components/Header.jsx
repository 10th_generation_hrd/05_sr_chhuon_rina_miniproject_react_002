import React, { Component } from 'react'
import { Button, Container, Form, FormControl, Nav, Navbar } from 'react-bootstrap'
import { Link } from "react-router-dom";


export default class Header extends Component {
  render() {
    return (
      <div>
        <Navbar bg="light" expand="lg">
        <Container>
          
          <Navbar.Brand style={{ marginLeft: "10px" }} href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="justify-content-end flex-grow-1 pe-3">
              <Nav.Link as={Link} to='/'>Home</Nav.Link>
              <Nav.Link as={Link} to='/category'>Category</Nav.Link>              
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      </div>
    )
  }
}

