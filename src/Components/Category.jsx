import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { Button, Col, Container, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router'
import { myApi } from '../api/myApi';
import CardCategory from './CardCategory';

export default function Category() {
    const navigate = useNavigate();
    const [data, setData] = useState([]);

    useEffect(() => {
        myApi.get("/category").then((res) => {
            console.log(res)
          setData(res.data.payload);
        });
      },);

    //   const handleDelete = (id) => {
    //     const newData = data.filter(data => data._id !== id)
    //     setData(newData)
    //     myApi.delete(`/category/${id}`).then((r) => {console.log(r.data.message);})
    //   }
    return (

        <div>
            <Container className='mt-2'>
                <Row>
                    <Col><h2>All Category</h2></Col>
                    <Col style={{ textAlign: "end" }}>
                        <Button variant="outline-dark" onClick={() => { navigate('/category/createcategory') }}><h5>New Category</h5></Button>
                    </Col>
                </Row>
                <Row>{data.map((item, index) => (
                    <Col key={index} className='col-2'>
                        <CardCategory/>
                    </Col>))}
                </Row>
            </Container>
        </div>
    )
}
