import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { Button, Container, Form } from 'react-bootstrap'
import { myApi } from '../api/myApi'

export default function CreateCategory() {
    const [title, setTitle] = useState("");
    const [data, setData] = useState([]);
    
    const handleSubmit = () => {
        myApi.post("/category",{title})
            .then((response) => console.log(response.data.message))
        }
        useEffect(() => {
        myApi.get("/category").then((res) => {
          setData(res.data.payload);
        });
      },);
    const handleTitle = (a) => {
        setTitle(a.target.value)
    }

  return (
    <div>
        <Container style={{ height: '1000px' }}>
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" onChange={handleTitle} placeholder="Enter Title" />
                </Form.Group>
                <Button className='mt-3'  onClick={handleSubmit} variant="primary">
                    Submit
                </Button>
            </Form>

        </Container>
    </div>
  )
}
