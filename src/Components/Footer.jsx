import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'

export default function Footer() {
  return (
    <div>
        <Container>
        <Row>
          <Col>
            <img src="https://i.pinimg.com/originals/00/22/d9/0022d90caf1ed22bc0b76970329caa7b.jpg" style={{ width: 90, heigth: 100 }} alt="" />
          </Col>
          <Col>
            <h5>Address</h5>
            <small><strong>Address:</strong> #12, St323, Sangket Boeung Kak II, Khan Toul Louk, Phnom Penh, Cambodia.</small>
          </Col>
          <Col>
            <h5>Contact</h5>
            <small><strong>Tel:</strong> 012 998 919(Khmer)</small>
            <br />
            <small><strong>Tel:</strong> 085 402 605(Korean)</small>
            <br />
            <small><strong>Email:</strong> info.kshrd@gamil.com phirum.gm@gamil.com</small>
          </Col>
        </Row>
      </Container>
    </div>
  )
}
