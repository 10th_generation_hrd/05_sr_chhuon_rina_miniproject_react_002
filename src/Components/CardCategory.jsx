import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { Button, Card, Container } from 'react-bootstrap'
import { useNavigate } from 'react-router';
import { myApi } from '../api/myApi';

export default function CardCategory(item ) {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    
    useEffect(() => {
        myApi.get("/category").then((res) => {
            console.log(res)
          setData(res.data.payload);
        });
      },);
    const handleDelete = (id) => {
        const newData = data.filter(data => data._id !== id)
        setData(newData)
        myApi.delete(`/category/${id}`).then((r) => {console.log(r.data.message);})
      }
    return (
        <div>
            <Container className='mt-2 '>
                <Card style={{ width: "12rem " }}>                    
                    <Card.Body>
                        <Card.Title style={{ textAlign: 'center' }}>{item.title}</Card.Title>
                        <Button variant="primary" className='mt-1'>Edit</Button>
                        <Button variant="warning" className='mt-1' onClick={() => { navigate('/category/view') }}>View</Button>
                        <Button variant="danger" className='mt-1'  onClick={() => handleDelete(item._id)} >Delete</Button>
                    </Card.Body>
                </Card>
            </Container>
        </div>
    )
}
