import logo from './logo.svg';
import './App.css';
import Header from './Components/Header';
import { Route, Router ,Routes} from 'react-router-dom';
import AllArticles from './Components/AllArticles';
import CardComponents from './Components/CardComponents';
import Body from './Components/Body';
import Footer from './Components/Footer';
import CreatePost from './Components/CreatePost';
import { useState } from 'react';
import { useEffect } from 'react';
import { myApi } from './api/myApi';
import View from './Components/View';
import Category from './Components/Category';
import CreateCategory from './Components/CreateCategory';
import ViewCategory from './Components/ViewCategory';


function App() {

  return (
    <div>
      <Header/>
      <Routes>
        <Route path='/' element={<Body/>}></Route>
        <Route path='/category' element={<Category/>}></Route>
        <Route path='/articles/createpost' element={<CreatePost/>}></Route>
        <Route path='/articles/view' element={<View/>}></Route>
        <Route path='/category/view' element={<ViewCategory/>}></Route>
        <Route path='/category/createcategory' element={<CreateCategory/>}></Route>
      </Routes>
      <Footer/>
    </div>
  );
}

export default App;
